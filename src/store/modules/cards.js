// mutations
const mutations = {
  INIT_ITEMS(state) {
    for(let i = 0; i < 8; i++) {
      state.items.push({
        value: i,
      })
    }
    state.items.push(...state.items)
  },
  ACTIVATE_ITEMS(state, data) {
    state.activeItems.push(data);
    if(state.activeItems.length === 8) {
      state.win = true
    }
  },
  RESET_GAME(state) {
    state.activeItems = [];
    state.items = [];
    state.win = false;
  }
};
const state = () => ({
  activeItems: [],
  items: [],
  win: false

});
export default {
  namespaced: true,
  mutations,
  state,
};
